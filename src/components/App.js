
import React from 'react';
import './style.scss'
import LoadingSpin from 'react-loading-spin';

const json = (response) => {
    return response.json()
}


class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            auth: false,
            name: '',
            pass: '',
            title: '',
            link: '',
            town: '',
            force: true,
            loading: true,
            cities: []
        }
    }


    componentDidMount() {
        let token = localStorage.getItem('token');

        chrome.tabs.query({ active: true, currentWindow: true }, (tab) => {
            this.setState({ link: tab[0].url, title: tab[0].title })
        });

        if (token) {
            this.setState({ auth: true })
        } else {
            this.setState({ auth: false })
        }

        this.getCitiesList();
        this.setState({ loading: false })
    }

    login() {
        const values = { phone: this.state.name, password: this.state.pass }
        this.setState({ loading: true })
        fetch("https://appraiser.mediatver.ru/api/site/login", {
            method: 'POST',
            mode: 'cors',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(values)
        }).then(status)
            .then(json)
            .then((data) => {
                if (data.token) {
                    localStorage.setItem('token', data.token);
                    this.setState({ auth: true })
                    this.setState({ loading: false })
                }
            })
            .catch((e) => {

                console.log(e)
                this.setState({ auth: false })
                this.setState({ loading: false })
            });
    }

    logout() {
        this.setState({ auth: false });
        localStorage.removeItem('token');
        this.forceUpdate();
    }

    getCitiesList = async () => {
        const token = localStorage.getItem('token');

        if (token !== null) {
            fetch(`https://appraiser.mediatver.ru/api/city`,
                {
                    method: 'GET',
                    mode: 'cors',
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    },
                })
                .then(status)
                .then(json)
                .then((data) => {
                    this.setState({ cities: data })
                })
                .catch((e) => {
                    console.log(e)
                });
        }
    }

    createRequest() {
        const token = localStorage.getItem('token');

        const values = { title: this.state.title, url: this.state.link, urgency: this.state.force ? 1 : 0, city_id: +this.state.town }
        this.setState({ loading: true })
        fetch('https://appraiser.mediatver.ru/api/card/create', {
            method: 'POST',
            mode: 'cors',
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(values)
        })
            .then(status)
            .then(json)
            .then((data) => {
                console.log(data)
                this.setState({ loading: false })
            })
            .catch((json) => {
                console.log(json)
                this.setState({ loading: true })
            });
    }

    render() {
        const { auth, loading } = this.state;

        return (
            <main className="main">
                <div className="container">
                    <section className="wrapper">
                        {loading ? <LoadingSpin /> : auth ?
                            <>
                                <div>
                                    <a type="button" className="logout-container" onClick={() => this.logout()}>Выход</a>
                                </div>
                                <form name="login" className="form">
                                    <div className="input-control">
                                        <label className="input-label">Наименование</label>
                                        <input value={this.state.title} onChange={(e) => { this.setState({ title: e.target.value }) }} name="email" className="input-field" placeholder="Наименование" />
                                    </div>
                                    <div className="input-control">
                                        <label className="input-label">Ссылка</label>
                                        <input value={this.state.link} onChange={(e) => { this.setState({ link: e.target.value }) }} className="input-field" placeholder="Ссылка" />
                                    </div>
                                    <div className="input-control">
                                        <label className="input-label">Город</label>
                                        <select className="select-css" onChange={(e) => {
                                            this.setState({ town: e.target.value })
                                        }}>
                                            {this.state.cities.map((item) => {
                                                return (<option key={item.id} custom_key={item.id} value={item.id}>{item.title}</option>)
                                            })}
                                        </select>
                                        {/* <input value={this.state.town} onChange={(e) => { this.setState({ town: e.target.value }) }} className="input-field" placeholder="Город" /> */}
                                    </div>
                                    <div className="input-control">
                                        <div>
                                            <input style={{ width: 15 }} checked={this.state.force} type="checkbox" onChange={(e) => { this.setState({ town: e.target.checked }) }} className="input-field" />
                                            <label style={{ marginLeft: 7 }} className="input-label">Срочно</label>
                                        </div>
                                    </div>
                                    <div className="input-control" style={{ justifyContent: 'center' }}>
                                        <input type="button" onClick={() => this.createRequest()} className="input-submit" value="Создать" />
                                    </div>
                                </form>
                            </> :
                            <form name="login" className="form">
                                <div className="input-control">
                                    <label className="input-label" hidden>Номер телефона</label>
                                    <input value={this.state.name} onChange={(e) => { this.setState({ name: e.target.value }) }} type="email" name="email" className="input-field" placeholder="Номер телефона" />
                                </div>
                                <div className="input-control">
                                    <label className="input-label" hidden>Пароль</label>
                                    <input value={this.state.pass} onChange={(e) => { this.setState({ pass: e.target.value }) }} type="password" name="password" className="input-field" placeholder="Пароль" />
                                </div>
                                <div className="input-control" style={{ justifyContent: 'center' }}>
                                    <input type="button" onClick={() => this.login()} className="input-submit" value="Войти" />
                                </div>
                            </form>
                        }
                    </section>
                </div>
            </main >
        )
    }
}


export default App;