var path = require('path');
var webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


module.exports = {
    devtool: 'eval',
    mode: 'development',
    entry: [
        "@babel/polyfill",
        './src/index'
    ],
    output: {
        path: path.join(__dirname, "/dist/"),
        filename: "popup.js",
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
    },
    //devtool: 'cheap-module-source-map',
    module: {
        rules: [{
            test: /\.(js|jsx|tsx|ts)$/,
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader',
                options: {
                    plugins: [
                        ['lodash'],
                        ['import', {
                            libraryName: "antd",
                            style: true
                        }],
                        ["@babel/plugin-proposal-optional-chaining"]
                    ],
                    presets: [
                        ['env', {
                            modules: false,
                            targets: {
                                node: 7
                            }
                        }]
                    ]
                }
            },
            {
                loader: 'eslint-loader'
            }
            ]
        },

        {
            test: /\.(less)$/,
            use: [
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                },
                {
                    loader: "less-loader",
                    options: {
                        lessOptions: {
                            javascriptEnabled: true,
                            modifyVars: {

                            }
                        }
                    },
                }
            ]
        },
        {
            test: /\.json$/,
            exclude: /node_modules/,
            loader: "json-loader"
        },
        {
            test: /\.(scss|css)$/,
            use: [
                MiniCssExtractPlugin.loader, // creates style nodes from JS strings
                "css-loader", // translates CSS into CommonJS
                "sass-loader" // compiles Sass to CSS, using Node Sass by default
            ]
        },
        {
            test: /\.jpe?g$|\.gif$|\.ico$|\.png$|\.svg$/,
            use: 'file-loader?name=[name].[ext]?[hash]'
        },

        {
            test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: 'url-loader?limit=10000&mimetype=application/font-woff'
        },
        {
            test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: 'file-loader'
        },
        {
            test: /\.otf(\?.*)?$/,
            use: 'file-loader?name=/fonts/[name].[ext]&mimetype=application/font-otf'
        },
        {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            use: [{
                loader: 'babel-loader',
            },
            {
                loader: '@svgr/webpack',
                options: {
                    babel: false,
                    icon: true,
                },
            },
            ],
        }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({})
    ],
    optimization: {
    },
};